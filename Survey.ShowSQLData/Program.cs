﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Survey.Core;
using SurveyRepository;
using Survey.IRepository;
using Ninject;
using Survey.AppService;

namespace Survey.ShowSQLData
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel container = new StandardKernel();
            container.Bind<ISurveyRepository>().To<MockSurveyRepository>();

            Survey.AppService.Data repo = container.Get<Survey.AppService.Data>();

            foreach (Survey.Core.Survey sur in repo.AllSurveys())
            {
                Console.WriteLine(sur.Title);
            }
                
            Console.ReadLine();
        }
    }


}
