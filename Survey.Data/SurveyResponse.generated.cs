#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the ClassGenerator.ttinclude code generation file.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;
using Telerik.OpenAccess;
using Telerik.OpenAccess.Metadata;
using Telerik.OpenAccess.Data.Common;
using Telerik.OpenAccess.Metadata.Fluent;
using Telerik.OpenAccess.Metadata.Fluent.Advanced;
using Survey.Data;

namespace Survey.Data	
{
	public partial class SurveyResponse
	{
		private int _surveyID;
		public virtual int SurveyID
		{
			get
			{
				return this._surveyID;
			}
			set
			{
				this._surveyID = value;
			}
		}
		
		private int _filledBy;
		public virtual int FilledBy
		{
			get
			{
				return this._filledBy;
			}
			set
			{
				this._filledBy = value;
			}
		}
		
		private int _surveyResponseID;
		public virtual int SurveyResponseID
		{
			get
			{
				return this._surveyResponseID;
			}
			set
			{
				this._surveyResponseID = value;
			}
		}
		
		private string _comments;
		public virtual string Comments
		{
			get
			{
				return this._comments;
			}
			set
			{
				this._comments = value;
			}
		}
		
		private Survey _survey;
		public virtual Survey Survey
		{
			get
			{
				return this._survey;
			}
			set
			{
				this._survey = value;
			}
		}
		
		private IList<QuestionResponse> _questionResponses = new List<QuestionResponse>();
		public virtual IList<QuestionResponse> QuestionResponses
		{
			get
			{
				return this._questionResponses;
			}
		}
		
	}
}
#pragma warning restore 1591
