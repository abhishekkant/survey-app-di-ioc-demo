#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the ClassGenerator.ttinclude code generation file.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;
using Telerik.OpenAccess;
using Telerik.OpenAccess.Metadata;
using Telerik.OpenAccess.Data.Common;
using Telerik.OpenAccess.Metadata.Fluent;
using Telerik.OpenAccess.Metadata.Fluent.Advanced;
using Survey.Data;

namespace Survey.Data	
{
	public partial class Survey
	{
		private int _iD;
		public virtual int ID
		{
			get
			{
				return this._iD;
			}
			set
			{
				this._iD = value;
			}
		}
		
		private string _title;
		public virtual string Title
		{
			get
			{
				return this._title;
			}
			set
			{
				this._title = value;
			}
		}
		
		private string _description;
		public virtual string Description
		{
			get
			{
				return this._description;
			}
			set
			{
				this._description = value;
			}
		}
		
		private DateTime _createdOn;
		public virtual DateTime CreatedOn
		{
			get
			{
				return this._createdOn;
			}
			set
			{
				this._createdOn = value;
			}
		}
		
		private DateTime? _expiresOn;
		public virtual DateTime? ExpiresOn
		{
			get
			{
				return this._expiresOn;
			}
			set
			{
				this._expiresOn = value;
			}
		}
		
		private int _createdBy;
		public virtual int CreatedBy
		{
			get
			{
				return this._createdBy;
			}
			set
			{
				this._createdBy = value;
			}
		}
		
		private bool _active;
		public virtual bool Active
		{
			get
			{
				return this._active;
			}
			set
			{
				this._active = value;
			}
		}
		
		private IList<SurveyResponse> _surveyResponses = new List<SurveyResponse>();
		public virtual IList<SurveyResponse> SurveyResponses
		{
			get
			{
				return this._surveyResponses;
			}
		}
		
		private IList<Survey_Question> _survey_Questions = new List<Survey_Question>();
		public virtual IList<Survey_Question> Survey_Questions
		{
			get
			{
				return this._survey_Questions;
			}
		}
		
	}
}
#pragma warning restore 1591
