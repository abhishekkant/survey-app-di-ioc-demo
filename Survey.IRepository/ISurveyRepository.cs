﻿using System;
using System.Collections.Generic;
using System.Linq;
using Survey.Core;
using System.Threading.Tasks;

namespace Survey.IRepository
{
    public interface ISurveyRepository
    {
        List<Survey.Core.Survey> GetAllSurveys();
        //Task<Survey.Core.Survey> GetSurvey(string surveyID);
        List<SurveyQuestion> GetSurvey(string surveyID);
        int SubmitResults(SurveyResponse surveyresults);
        //int CreateSurvey(Survey.Core.Survey newSurvey);
        List<ResResults> GetResults(int surveyid, int quesid);
        
    }
  
 
}
