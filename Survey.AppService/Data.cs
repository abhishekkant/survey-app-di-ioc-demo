﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Survey.IRepository;


namespace Survey.AppService
{
    public class Data
    {
        ISurveyRepository repo;
        public Data(ISurveyRepository inrepo)
        {
            repo = inrepo;
        }

        public List<Survey.Core.Survey> AllSurveys()
        {

            return this.repo.GetAllSurveys();
        }
    }
}
