﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Survey.Data;
using Survey.Core;
using AutoMapper;
using Survey.IRepository;

namespace SurveyRepository
{
    public class SqlSurveyRepository : ISurveyRepository
    {

        SurveysModel sdx = new SurveysModel();

        public SqlSurveyRepository()
        {
            Mapper.CreateMap<Survey.Data.Survey,Survey.Core.Survey>()
                .ForMember(des => des.Surveyid,s => s.MapFrom(db => db.ID))
                .ForMember(des => des.Validtill, s => s.MapFrom(db => db.ExpiresOn));
            //Mapper.CreateMap<Survey.Data.Survey_Question, Survey.Core.SurveyQuestion>();
            //Mapper.CreateMap<Survey.Data.SurveyResponse, Survey.Core.SurveyResponse>();

        }

        public List<Survey.Core.Survey> GetAllSurveys()
        {
            
            var activesurveys = from s in sdx.Surveys
                                where s.Active == true
                                select s;

            if (activesurveys != null)
            {

                List<Survey.Core.Survey> actvsurveys = new List<Survey.Core.Survey>();
                actvsurveys = Mapper.Map<List<Survey.Core.Survey>>(activesurveys.ToList());
                return actvsurveys;
            }
            else
                return null;
        }

        public List<SurveyQuestion> GetSurvey(string surveyID)
        {
            //sdx.FetchStrategy.LoadWith()
            var surques = (from q in sdx.Survey_Questions
                          where q.SurveyID == Convert.ToInt32(surveyID)
                          orderby q.OrderId
                          select q); //.Include("Questions").Include("QuestionOptions");
            if (surques != null)
            {
                List<SurveyQuestion> ques = new List<SurveyQuestion>();

                foreach (Survey_Question sq in surques.ToList())
                {
                    SurveyQuestion sques = new SurveyQuestion();
                    sques.id = sq.QuestionID;
                    sques.text = sq.Question.Text;
                    sques.Type = "likertscale";
                    List<string> qopts = new List<string>();
                    var qos = from qop in sdx.QuestionOptions
                              where qop.QuestionID == sq.QuestionID
                              select qop.OptionText;
                    foreach (string optiontext in qos.ToList())
                    {
                        qopts.Add(optiontext);
                    }
                    sques.options = qopts;
                    ques.Add(sques);
                }
                return ques;
            }
            else
                return null;
        }

        public int SubmitResults(Survey.Core.SurveyResponse surveyresults)
        {
            Survey.Data.SurveyResponse sr = new Survey.Data.SurveyResponse();
            sr.SurveyID = Convert.ToInt32(surveyresults.surveyid);
            sr.Comments = surveyresults.comments;
            foreach (Survey.Core.Responses r in surveyresults.surveyResponses)
            {
                Survey.Data.QuestionResponse qr = new Survey.Data.QuestionResponse();
                
                qr.ResponseText = r.resvalue;
                qr.QuesID = r.qid;

                qr.SurveyResponse = sr;
                sr.QuestionResponses.Add(qr);

            }
            sdx.Add(sr);
            try
            {
                sdx.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public List<ResResults> GetResults(int surveyid, int quesid)
        {
            var results = from qr in sdx.QuestionResponses
                          where qr.SurveyResponse.SurveyID == surveyid && qr.QuesID == quesid
                          select qr;
            var grpresults = from gr in results
                             group gr by gr.ResponseText into g
                             select new { Option = g.Key, count = g.Count() };

            List<ResResults> fResults = new List<ResResults>();
            foreach (var iopt in grpresults.ToList())
            {
                ResResults rr = new ResResults();
                rr.ResOption = iopt.Option;
                rr.ResTotal = iopt.count;
                fResults.Add(rr);
            }
            return fResults;
        }
    }
}
