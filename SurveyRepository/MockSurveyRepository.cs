﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Survey.Core;
using Survey.IRepository;
using Telerik.JustMock;
using Telerik.JustMock.Helpers;

namespace SurveyRepository
{
    public class MockSurveyRepository: ISurveyRepository
    {
        public List<Survey.Core.Survey> GetAllSurveys()
        {

            List<Survey.Core.Survey> mockSurveys = new List<Survey.Core.Survey>()
                {
                    new Survey.Core.Survey(){Title = "DI Survey", Createdby="Abhishek"},
                    new Survey.Core.Survey() { Title = "IoC Container", Createdby = "Abhishek Kant" },
                    new Survey.Core.Survey() { Title = "DI MVC Container", Createdby = "Lohith GN" }

                };


            ISurveyRepository mockRepository = Mock.Create<ISurveyRepository>();
            mockRepository.Arrange(x => x.GetAllSurveys()).Returns(mockSurveys);
            return mockRepository.GetAllSurveys();    
                
                

        }

        public List<SurveyQuestion> GetSurvey(string surveyID)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public int SubmitResults(SurveyResponse surveyresults)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public List<ResResults> GetResults(int surveyid, int quesid)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
