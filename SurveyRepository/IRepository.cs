﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyRepository
{
    interface IRepository<T>
    {
        void GetSurvey(int ID);
        void GetSurvey(string surveyID);
        void GetUser(string username);
        void AddResponse();
    }
}
