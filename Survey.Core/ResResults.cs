﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class ResResults
    {
        string _resOption;
        int _resTotal;
        public int ResTotal
        {
            get
            {
                return this._resTotal;
            }
            set
            {
                this._resTotal = value;
            }
        }

        public string ResOption
        {
            get
            {
                return this._resOption;
            }
            set
            {
                this._resOption = value;
            }
        }
    }
}
