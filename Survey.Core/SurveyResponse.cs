﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class SurveyResponse
    {
        string _surveyid;
        string _comments;
                    DateTime _responsedate;
                    List<Responses> _surveyResponses;
       
        public List<Responses> surveyResponses
        {
            get
            {
                return this._surveyResponses;
            }
            set
            {
                this._surveyResponses = value;
            }
        }

        public DateTime responsedate
        {
            get
            {
                return this._responsedate;
            }
            set
            {
                this._responsedate = value;
            }
        }

        public string comments
        {
            get
            {
                return this._comments;
            }
            set
            {
                this._comments = value;
            }
        }

        public string surveyid
        {
            get
            {
                return this._surveyid;
            }
            set
            {
                this._surveyid = value;
            }
        }
    }

    public class Responses
    {
        int _qid;
        string _resvalue;
        
        public string resvalue
        {
            get
            {
                return this._resvalue;
            }
            set
            {
                this._resvalue = value;
            }
        }

        public int qid
        {
            get
            {
                return this._qid;
            }
            set
            {
                this._qid = value;
            }
        }
    }
}
