﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class Survey
    {
        string surveyid;
        string type;
        string title;
        DateTime createdon;
        DateTime validtill;
        string createdby;
        bool active;
        List<SurveyQuestion> questions;

        public List<SurveyQuestion> Questions
        {
            get
            {
                return this.questions;
            }
            set
            {
                this.questions = value;
            }
        }

        public bool Active
        {
            get
            {
                return this.active;
            }
            set
            {
                this.active = value;
            }
        }

        public string Createdby
        {
            get
            {
                return this.createdby;
            }
            set
            {
                this.createdby = value;
            }
        }

        public DateTime Validtill
        {
            get
            {
                return this.validtill;
            }
            set
            {
                this.validtill = value;
            }
        }

        public DateTime Createdon
        {
            get
            {
                return this.createdon;
            }
            set
            {
                this.createdon = value;
            }
        }

        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                this.title = value;
            }
        }

        public string Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
            }
        }

        public string Surveyid
        {
            get
            {
                return this.surveyid;
            }
            set
            {
                this.surveyid = value;
            }
        }
    }
}
