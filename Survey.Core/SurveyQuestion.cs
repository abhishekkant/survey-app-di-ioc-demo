﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Core
{
    public class SurveyQuestion
    {
        int qid;
        string description;
        string type;
        List<string> qoptions;
        public List<string> options
        {
            get
            {
                return this.qoptions;
            }
            set
            {
                this.qoptions = value;
            }
        }

        public string Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
            }
        }

        public string text
        {
            get
            {
                return this.description;
            }
            set
            {
                this.description = value;
            }
        }

        public int id
        {
            get
            {
                return this.qid;
            }
            set
            {
                this.qid = value;
            }
        }
    }
}
